﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReverseLinkedList
{
    class Program
    {
        static void Main(string[] args)
        {

            var length = Convert.ToInt32(args.Length > 0? args[0] :"10");
            var head = new Node<int>(0);
            var curr = head;
            for (int i = 1; i < length; i++)
            {
                curr.Next = new Node<int>(i);
                curr = curr.Next;
            }

            Console.Out.WriteLine("DumpList(head) = {0}", DumpList(head));

            var reversedHead = Reverse(head);

            Console.Out.WriteLine("DumpList(reversedHead) = {0}", DumpList(reversedHead));
        }

        private static string DumpList(Node<int> reversedHead)
        {
            var sb = new StringBuilder();
            while (reversedHead != null)
            {
                sb.Append(reversedHead.Value).Append(',');
                reversedHead = reversedHead.Next;
            }
            return sb.Replace(",", "\n", sb.Length - 1, 1).ToString();
        }

        static Node<T> Reverse<T>(Node<T> head)
        {
            var current = head.Next;
            head.Next = null;
            while (current != null)
            {
                var temp = current.Next;
                current.Next = head;
                head = current;
                current = temp;
            }
            return head;
        } 
    }
}
